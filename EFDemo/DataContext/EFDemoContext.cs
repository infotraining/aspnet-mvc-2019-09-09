﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DataContext
{
    public class EFDemoContext : DbContext
    {
        public DbSet<Programmer> Programmers { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Task> Tasks { get; set; }
    }
}
