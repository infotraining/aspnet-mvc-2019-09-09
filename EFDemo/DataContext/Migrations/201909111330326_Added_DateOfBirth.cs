namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_DateOfBirth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Programmers", "DateOfBirth", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Programmers", "DateOfBirth");
        }
    }
}
