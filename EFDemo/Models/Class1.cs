﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Programmer
    {
        public Programmer()
        {
            Tasks = new List<Task>();
        }

        public Programmer(string name, bool active, DateTime dateOfBirth, int teamId) : this()
        {
            Name = name;
            Active = active;
            DateOfBirth = dateOfBirth;
            TeamId = teamId;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }

    public class Task
    {

        public int Id { get; set; }
        public string Description { get; set; }
        [Required]
        public Programmer Programmer { get; set; }
        public Level Level { get; set; }
    }

    public class Team
    {
        public Team()
        {
            Programmers = new List<Programmer>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Programmer> Programmers { get; set; }
    }

    public enum Level
    {
        Easy,
        Medium,
        Hard
    }
}
