﻿using DataContext;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new NullDatabaseInitializer<EFDemoContext>());
            //InsertProgrammer();
            //SimpleQuery();
            //QueryAndUpdate();
            //QueryAndUpdateDisconnected();
            //QueryByFind();
            //QueryBySql();
            //DeletingDisconnected();
            //InsertProgrammerWithTasks();
            ProgrammerGraphQuery();

            Console.ReadLine();
        }

        public static void ProgrammerGraphQuery()
        {
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                //var programmer = context.Programmers.FirstOrDefault(x => x.Name == "Maria");

                //eager loading - ładowanie zachłanne
                //var programmer = context.Programmers.Include(x => x.Tasks).FirstOrDefault(x => x.Name == "Maria");

                var programmer = context.Programmers.FirstOrDefault(x => x.Name == "Maria");

                //context.Entry(programmer).Collection(x => x.Tasks).Load();

                Console.WriteLine(programmer.Name);

                //foreach (var item in programmer.Tasks)
                //{
                //    Console.WriteLine(item.Description);
                //}


            }
        }

        public static void InsertProgrammerWithTasks()
        {
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                var programmer = new Programmer("Maria", true, new DateTime(2000, 1, 1), 1);

                var task1 = new Task { Id = 1, Description = "Implement Repository" };

                var task2 = new Task { Id = 2, Description = "Implement UnitOfWork" };

                context.Programmers.Add(programmer);

                programmer.Tasks.Add(task1);
                programmer.Tasks.Add(task2);

                context.SaveChanges();
            }
        }

        public static void DeletingDisconnected()
        {
            Programmer programmer;
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);
                //programmer = context.Programmers.Find(1);
            }

            int idToDelete = 2;

            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                //context.Programmers.Attach(programmer);
                //context.Programmers.Remove(programmer);

                //context.Entry(programmer).State = EntityState.Deleted;

                //var toDelete = context.Programmers.Find(idToDelete);
                //context.Programmers.Remove(toDelete);

                context.Database.ExecuteSqlCommand("exec DeleteProgrammerById {0}", 3);

                context.SaveChanges();
            }
        }

        public static void QueryBySql()
        {
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                var programmers = 
                    context.Programmers.SqlQuery("exec GetYoungProgrammers");

                foreach (var item in programmers)
                {
                    Console.WriteLine(item.Name);
                }
            }
        }

        public static void QueryByFind()
        {
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                var keyval = 2;

                var programmer = context.Programmers.AsNoTracking().First(x => x.Id == keyval);

                Console.WriteLine(programmer.Name);

                var programmmer2 = context.Programmers.Find(keyval);

                Console.WriteLine(programmmer2.Name);

            }
        }

        public static void QueryAndUpdateDisconnected()
        {
            Programmer programmer;

            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);
                programmer = context.Programmers.FirstOrDefault();
            }

            programmer.Name = "Zenon";

            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);
                //context.Programmers.Add(programmer);

                context.Programmers.Attach(programmer);
                context.Entry(programmer).State = EntityState.Modified;

                context.SaveChanges();
            }
        }

        public static void QueryAndUpdate()
        {
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                var programmer = context.Programmers.FirstOrDefault();

                programmer.Name = "Roman";

                context.SaveChanges();
            }
        }

        public static void SimpleQuery()
        {
            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                //var programmer = context.Programmers.Single(x => x.Name == "Robert");

                var programmers = context.Programmers.Where(x => x.Id > 2).ToList();

                programmers = programmers.Where(x => x.TeamId == 1).ToList();

                foreach (var item in programmers)
                {
                    Console.WriteLine(item.Name);
                }

            }

        }

        public static void InsertProgrammer()
        {
            var programmer = new Programmer("Adam", false, new DateTime(1995, 1, 1), 1);

            using (var context = new EFDemoContext())
            {
                context.Database.Log = s => Console.WriteLine(s);

                context.Programmers.Add(programmer);

                context.SaveChanges();
            }
        }
    }
}
