﻿namespace ViewsAndHelperMethods.Models
{
    public enum Role
    {
        None,
        Editor,
        Admin
    }
}