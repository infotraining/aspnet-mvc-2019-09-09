﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ViewsAndHelperMethods.Infrastructure;

namespace ViewsAndHelperMethods.Models.Metadata
{
    public class UserMetadata
    {
        //[HiddenInput(DisplayValue = false)]
        [ShowOnlyForDisplay]
        public int Id { get; set; }
        [DisplayName("Podaj imię")]
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }
        public Address Address { get; set; }
        [UIHint("EnumTest")]
        public Gender Gender { get; set; }
        public Role Role { get; set; }
        public bool Verified { get; set; }
    }
}