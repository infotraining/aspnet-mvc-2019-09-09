﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewsAndHelperMethods.Infrastructure;
using ViewsAndHelperMethods.Models.Metadata;

namespace ViewsAndHelperMethods.Models
{
    [MetadataType(typeof(UserMetadata))]
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Address Address { get; set; }
        public Role Role { get; set; }
        public Gender Gender { get; set; }
        public bool Verified { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
}