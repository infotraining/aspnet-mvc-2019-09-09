﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewsAndHelperMethods.Infrastructure
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ShowOnlyForDisplayAttribute : Attribute, IMetadataAware
    {
        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.ShowForDisplay = true;
            metadata.ShowForEdit = false;
        }
    }
}