﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewsAndHelperMethods.Infrastructure
{
    public static class ExternalHelpers
    {
        public static MvcHtmlString ListArrayItems(this HtmlHelper html, string[] list)
        {
            TagBuilder tb = new TagBuilder("ul");

            foreach (var item in list)
            {
                TagBuilder itemTag = new TagBuilder("li");
                itemTag.SetInnerText(item);
                tb.InnerHtml += itemTag.ToString();
            }

            return new MvcHtmlString(html.Encode(tb.ToString()));
        }
    }
}