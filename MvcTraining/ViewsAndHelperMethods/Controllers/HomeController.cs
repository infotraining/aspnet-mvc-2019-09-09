﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewsAndHelperMethods.Models;

namespace ViewsAndHelperMethods.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Students = new string[] { "Łukasz", "Jakub", "Rafał", "Marek", "Paweł" };
            ViewBag.Courses = new string[] { "MVC 5", "C# 7.0", "Entity Framework 6" };

            return View();
        }

        public ActionResult Create()
        {
            var user = new User();
            return View(user);
        }

        [HttpPost]
        public ActionResult Create(User user)
        {
            return View("UserDetails", user);
        }
    }
}