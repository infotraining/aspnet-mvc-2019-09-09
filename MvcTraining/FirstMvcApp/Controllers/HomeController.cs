﻿using FirstMvcApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMvcApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Participant participant = new Participant
            {
                Confirmed = true,
                Name = "Marek",
                Phone = "500600700",
                Email = "lukasz.zawadzki@infotraining.pl"
            };

            ViewBag.Message = "Hello World! (from the controller)";

            return View(participant);
        }

        [HttpGet]
        public ActionResult Enroll()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Enroll(Participant participant)
        {
            if (ModelState.IsValid)
            {
                //dodatkowe operacje
                return View("Confirmation", participant);
            }

            return View(participant);
        }

        public ActionResult List()
        {
            List<Participant> participants = new List<Participant>
            {
                new Participant("Łukasz", "lukasz.zawadzki@infotrainig.pl", "500600700", true),
                new Participant("Robert", "robert@infotrainig.pl", "600700800", true),
                new Participant("Adam", "adam@infotrainig.pl", "700800900", true)
            };

            return View(participants);
        }

        [ChildActionOnly]
        public ActionResult CurrentTime()
        {
            string time = DateTime.Now.ToString();

            return PartialView("CurrentTime", time);
        }

        public ActionResult Test()
        {
            return View();
        }
    }
}