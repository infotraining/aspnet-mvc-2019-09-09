﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMvcApp.Models
{
    public class Participant
    {
        public Participant()
        {

        }

        public Participant(string name, string email, string phone, bool confirmed)
        {
            Name = name;
            Email = email;
            Phone = phone;
            Confirmed = confirmed;
        }

        [Required]
        [AllowHtml]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Pole z imieniem jest obowiązkowe")]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        //[Phone]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Niepoprawny telefon")]
        public string Phone { get; set; }
        [Required]
        public bool Confirmed { get; set; }
    }
}