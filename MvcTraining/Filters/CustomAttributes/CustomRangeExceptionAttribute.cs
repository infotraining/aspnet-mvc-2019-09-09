﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.CustomAttributes
{
    public class CustomRangeExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is ArgumentOutOfRangeException)
            {
                //filterContext.Result = new RedirectResult("~/Content/CustomRangeErrorPage.html");

                var value = ((ArgumentOutOfRangeException)filterContext.Exception).ActualValue.ToString();

                filterContext.Result = new ViewResult()
                {
                    ViewName = "CustomRangeError",
                    ViewData = new ViewDataDictionary<string>(value)
                };

                filterContext.ExceptionHandled = true;
            }
        }
    }
}