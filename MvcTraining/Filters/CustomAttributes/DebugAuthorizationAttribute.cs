﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.CustomAttributes
{
    public class DebugAuthorizationAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool disableAuthorization = false;

#if DEBUG
            disableAuthorization = true;
#endif

            if (disableAuthorization)
            {
                return true;
            }

            return base.AuthorizeCore(httpContext);
        }
    }
}