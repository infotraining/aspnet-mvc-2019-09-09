﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filters.CustomAttributes
{
    public class ActionTimerAttribute : FilterAttribute, IActionFilter
    {
        private Stopwatch _timer;

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _timer.Stop();

            if (filterContext.Exception == null)
            {
                string controller = filterContext.RequestContext.RouteData.Values["controller"].ToString();

                string action = filterContext.RequestContext.RouteData.Values["action"].ToString();

                string info = $"Controller: {controller}, Action: {action}, " +
                    $"Time: {_timer.Elapsed.TotalMilliseconds} ms";

                Debug.WriteLine(info);
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _timer = Stopwatch.StartNew();
        }
    }

    public class ResultTimerAttribute : FilterAttribute, IResultFilter, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            throw new NotImplementedException();
        }
    }

    public class ProfilerAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }
}