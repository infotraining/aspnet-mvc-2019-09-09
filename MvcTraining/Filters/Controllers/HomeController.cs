﻿using Filters.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Filters.Controllers
{
    //[DebugAuthorization]
    [ActionTimer]
    public class HomeController : Controller
    {
        //[Authorize]
        [SimpleMessage(Message = "1")]
        [SimpleMessage(Message = "2", Order = 4)]
        [SimpleMessage(Message = "3", Order = 3)]
        [SimpleMessage(Message = "4", Order = 2)]
        [SimpleMessage(Message = "5", Order = 1)]
        [OverrideActionFilters]
        public ActionResult Index()
        {
            //throw new Exception();

            return View();
        }

        [SpecialUser]
        public ActionResult GetSecretInfo()
        { 
            string info = "Kod nuklerany: 1234";

            return View(default, default, info);
        }

        //[CustomRangeException]
        [HandleError(ExceptionType = typeof(ArgumentOutOfRangeException), View = "CustomRangeError")]
        public ActionResult EnterCode(string id)
        {
            if (id.Length == 6)
            {
                return View(model: id);
            }
            else
            {
                throw new ArgumentOutOfRangeException("id", id, "Długość kodu musi wynosić 6!");
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Debug.WriteLine("Kolejny komunikat");
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }
}