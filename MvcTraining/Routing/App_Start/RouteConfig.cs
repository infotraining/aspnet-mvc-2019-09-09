﻿using Routing.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;
using System.Web.Routing;

namespace Routing
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //Route route2 = new Route("info", new RedirectRouteHandler());

            //routes.Add(route2);

            //routes.MapRoute(
            //    "", 
            //    "strona-domowa", 
            //    new { controller = "Home", action = "Index" }
            //    );

            //routes.MapRoute(
            //    "", 
            //    "User/{action}/{id}", 
            //    new { controller = "Home" }
            //    );

            //routes.MapRoute(
            //    "", 
            //    "Public{controller}/{action}", 
            //    new { controller = "Home", action = "Index" }, 
            //    new[] { "Routing.NewControllers" }
            //    );

            //routes.MapRoute(
            //    "", 
            //    "Main/{controller}/{action}", 
            //    new { controller = "Home", action = "Index" }
            //    );

            //var route = routes.MapRoute(
            //    name: "",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = "abcdef" },
            //    constraints: new
            //    {
            //        controller = "^H.*",
            //        action = "^Index$||^Details$",
            //        id = new CompoundRouteConstraint(new IRouteConstraint[] { new AlphaRouteConstraint(), new LengthRouteConstraint(6) }),
            //        customConstraint = new UserAgentConstraint()
            //    },
            //    namespaces: new[] { "Routing.Controllers" }
            //);

            routes.RouteExistingFiles = true;

            routes.Ignore("Content/{string}.xml");

            routes.MapRoute(
                name: "",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //route.DataTokens["UseNamespaceFallback"] = false;
        }
    }
}
