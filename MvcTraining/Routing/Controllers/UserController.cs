﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Routing.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            ViewBag.Controller = "User";
            ViewBag.Action = "Index";
            ViewBag.Id = RouteData.Values["id"];
            ViewBag.Catchall = RouteData.Values["number"];

            return View("Result");
        }
    }
}