﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Routing.Controllers
{
    [RoutePrefix("Main")]
    public class HomeController : Controller
    {
        [Route("~/")]
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "Index";
            ViewBag.Id = RouteData.Values["id"];
            ViewBag.Catchall = RouteData.Values["catchall"];

            string url = Url.Action("Details", new { id = "abcdef" });

            return View("Result");

            //return Redirect(url);

            //return RedirectToAction("Details", new { id = "abcdef" });
;        }

        [Route("Show-Details/{id:alpha:length(6)}")]
        public ActionResult Details(string id)
        {
            ViewBag.Controller = "Home";
            ViewBag.Action = "Details";
            ViewBag.Id = id;

            return View("Result");
        }
    }
}