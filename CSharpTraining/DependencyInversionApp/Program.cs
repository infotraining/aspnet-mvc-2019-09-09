﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();

            //car.SetEngine(new Engine());

            //car.Engine = new UpgradedEngine();

            //car.Run();

            car.Drive();

            Console.ReadLine();
        }
    }
}
