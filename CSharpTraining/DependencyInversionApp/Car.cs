﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversionApp
{
    public class Car
    {
        public string Name { get; set; }

        private IEngine _engine;

        //public Car(IEngine engine)
        //{
        //    _engine = engine;
        //}

        public void SetEngine(IEngine engine)
        {
            _engine = engine;
        }

        public IEngine Engine
        {
            get
            {
                if (_engine == null)
                    _engine = new Engine();
                return _engine;
            }
            set
            {
                _engine = value;
            }
        }

        public void Run()
        {
            Console.WriteLine("Samochód został uruchomiony");
            Engine.TurnOn();
        }

        public void Drive()
        {
            Engine.Accelarate();
        }
    }
}
