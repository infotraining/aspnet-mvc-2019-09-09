﻿using System;

namespace DependencyInversionApp
{
    public class Engine : IEngine
    {
        private const int _topSpeed = 180;

        private bool _isOn = false;

        public void TurnOn()
        {
            _isOn = true;
            Console.WriteLine("Silnik został włączony");
        }

        public void Accelarate()
        {
            if (!_isOn)
            {
                Console.WriteLine("Nie włączono silnika");
                return;
            }

            for (int i = 0; i <= _topSpeed; i += 10)
            {
                Console.WriteLine("> aktualna prędkość to " + i);
            }

            Console.WriteLine("Silnik osiagnął pełną prędkość");
        }
    }
}
