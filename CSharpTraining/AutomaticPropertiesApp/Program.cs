﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomaticPropertiesApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var employee = new Employee();

            //employee.Id = 1;

            //employee.Name = "Roman";

            var id = employee.Id;

            var name = employee.Name;

            Console.WriteLine(id + " " + name);

            //employee.Code = -7;

            //var code = employee.Code;

            /*Console.WriteLine(code);*/

            Console.ReadLine();
        }
    }
}
