﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesApp
{
    public delegate bool IsPromotable(Employee employee);

    public static class EmployeeHelper
    {
        public static void CheckForPromotions(List<Employee> employees)
        {
            foreach (var employee in employees)
            {
                if (employee.Grade > PerformanceGrade.Average)
                {
                    Console.WriteLine($"{employee.Name} zasługuje na awans!");
                }
            }
        }

        public static void BetterCheckForPromotions(List<Employee> employees, IsPromotable promotable)
        {
            foreach (var employee in employees)
            {
                if (promotable(employee))
                {
                    Console.WriteLine($"{employee.Name} zasługuje na awans!");
                }
            }
        }

        public static void BestCheckForPromotions(List<Employee> employees, Predicate<Employee> promotable)
        {
            foreach (var employee in employees)
            {
                if (promotable(employee))
                {
                    Console.WriteLine($"{employee.Name} zasługuje na awans!");
                }
            }
        }
    }
}
