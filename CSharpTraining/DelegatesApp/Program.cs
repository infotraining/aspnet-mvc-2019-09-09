﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesApp
{
    delegate void CustomDelegate(string text);

    class Program
    {
        static void Main(string[] args)
        {
            //CustomDelegate del = new CustomDelegate(Display);

            //del("Wiadomość z delegata...");

            List<Employee> employees = new List<Employee>
            {
                new Employee(1, "Bartosz", 6000M, PerformanceGrade.Weak, 5),
                new Employee(2, "Maciek", 4000M, PerformanceGrade.Good, 2),
                new Employee(3, "Roman", 5000M, PerformanceGrade.Terrible, 11),
                new Employee(4, "Grzesiek", 2000M, PerformanceGrade.Excellent, 7)
            };

            //EmployeeHelper.CheckForPromotions(employees);

            IsPromotable delProm = new IsPromotable(CustomPromotionsLogic);

            //EmployeeHelper.BetterCheckForPromotions(employees, delProm);

            //EmployeeHelper.BetterCheckForPromotions(employees,
            //    delegate (Employee employee) { return employee.Grade > PerformanceGrade.Average; });

            EmployeeHelper.BestCheckForPromotions(employees, e => e.Grade > PerformanceGrade.Average);

            employees.ForEach(emp => emp.Id += 10);

            foreach (var item in employees)
            {
                Console.WriteLine(item.Id);
            }

            int a = 10;
            int b = 15;
            int c = 20;

            Func<int, int, int, string> selector = (p1, p2, p3) => "Iloczyn tych liczb to: " + (p1 * p2 * p3).ToString();

            string result = selector(a, b, c);

            Console.WriteLine(result);

            var employeesName = employees.Select(emp => emp.Name);

            foreach (var item in employeesName)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        public static bool CustomPromotionsLogic(Employee employee)
        {
            return employee.Grade > PerformanceGrade.Average && employee.YearsSinceHire > 3;
        }

        static void Display(string displayText)
        {
            Console.WriteLine($"\t{displayText}");
        }

        static void Message(string message)
        {
            Console.WriteLine(message);
        }
    }
}
