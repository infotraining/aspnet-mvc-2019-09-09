﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public enum PerformanceGrade
    {
        Terrible,
        Weak,
        Average,
        Good,
        Excellent
    }

    public class Employee
    {
        public Employee()
        {
        }

        public Employee(int id, string name, decimal salary, PerformanceGrade grade, int yearsSinceHire)
        {
            Id = id;
            Name = name;
            Salary = salary;
            Grade = grade;
            YearsSinceHire = yearsSinceHire;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
        public PerformanceGrade Grade { get; set; }
        public int YearsSinceHire { get; set; }
    }

    //public class Employee
    //{
    //    public int Id;
    //    public string Name { get; set; }

    //    private int _code = 10;

    //    public int Code
    //    {
    //        get
    //        {
    //            return _code;
    //        }
    //        set
    //        {
    //            if (value > 0)
    //                _code = value;
    //        }
    //    }

    //    public int GetCode()
    //    {
    //        return _code;
    //    }

    //    public void SetCode(int code)
    //    {
    //        if (code > 0)
    //        {
    //            _code = code;
    //        }
    //    }
    //}
}
