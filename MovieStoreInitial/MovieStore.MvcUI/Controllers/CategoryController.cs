﻿using MovieStore.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.MvcUI.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoriesRepository _categoriesRepo;

        public CategoryController(ICategoriesRepository categoriesRepo)
        {
            _categoriesRepo = categoriesRepo;
        }

        [ChildActionOnly]
        public ActionResult CategoriesList()
        {
            var categories = _categoriesRepo.GetAllIncluding(x => x.Movies);

            return View(categories);
        }
    }
}