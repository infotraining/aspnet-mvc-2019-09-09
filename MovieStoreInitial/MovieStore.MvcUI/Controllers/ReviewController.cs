﻿using MovieStore.BusinessLogicLayer.Interfaces;
using MovieStore.BusinessLogicLayer.Messaging;
using MovieStore.BusinessLogicLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.MvcUI.Controllers
{
    public class ReviewController : Controller
    {
        private readonly IReviewsService _reviewsService;

        public ReviewController(IReviewsService reviewsService)
        {
            _reviewsService = reviewsService;
        }

        public ActionResult AddReview(int id, string title)
        {
            var model = new AddReviewViewModel { MovieId = id, Title = title };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddReview(AddReviewViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var request = new GetAddReviewRequest { Review = model };

            var response = _reviewsService.AddReview(request);

            if (response.IsSuccess)
            {
                return RedirectToAction("Details", "Home", new { id = model.MovieId });
            }

            ModelState.AddModelError("", response.Message);

            return View(model);
        }
    }
}