﻿using MovieStore.BusinessLogicLayer.Interfaces;
using MovieStore.BusinessLogicLayer.Messaging;
using MovieStore.BusinessLogicLayer.ViewModels;
using MovieStore.DataAccessLayer;
using MovieStore.DataAccessLayer.Database;
using MovieStore.DataAccessLayer.Repositories;
using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.MvcUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMoviesService _moviesService;

        public HomeController(IMoviesService moviesService)
        {
            _moviesService = moviesService;
        }

        public ActionResult Index(string id = "All", int page = 1, int itemsPerPage = 20)
        {
            var request = new GetMovieListRequest { PagingInfo = new PagingViewModel(id, page, itemsPerPage) };

            var response = _moviesService.GetMovies(request);

            return View(response);
        }

        public ActionResult Details(int id)
        {
            var response = _moviesService.GetMovie(new GetMovieDetailsRequest { Id = id });

            return View(response);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}