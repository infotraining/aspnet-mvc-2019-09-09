﻿using MovieStore.BusinessLogicLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MovieStore.MvcUI.Helpers
{
    public static class CustomHelpers
    {
        public static MvcHtmlString PageLinks
            (this HtmlHelper html, PagingViewModel paging, Func<int, string> urlPage)
        {
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 1; i <= paging.TotalPages; i++)
            {
                TagBuilder tBuilder = new TagBuilder("a");

                tBuilder.MergeAttribute("href", urlPage(i));

                tBuilder.InnerHtml = i.ToString();

                if (i == paging.CurrenPage)
                {
                    tBuilder.AddCssClass("btn-primary selected");
                }

                tBuilder.AddCssClass("btn btn-default");

                sBuilder.Append(tBuilder.ToString());
            }

            return MvcHtmlString.Create(sBuilder.ToString());
        }
    }
}