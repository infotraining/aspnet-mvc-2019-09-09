﻿using Autofac;
using Autofac.Integration.Mvc;
using MovieStore.BusinessLogicLayer;
using MovieStore.BusinessLogicLayer.Helpers;
using MovieStore.BusinessLogicLayer.Interfaces;
using MovieStore.DataAccessLayer;
using MovieStore.DataAccessLayer.Database;
using MovieStore.DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MovieStore.MvcUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MvcApplication.ConfigureAutofac();
            AutomapperConfig.ConfigureMappings();
        }

        private static void ConfigureAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<MovieStoreContext>().As<DbContext>();

            builder.RegisterType<MoviesRepository>().As<IMoviesRepository>();
            builder.RegisterType<CategoriesRepository>().As<ICategoriesRepository>();

            builder.RegisterGeneric(typeof(GenericRepository<>)).As(typeof(IGenericRepository<>));

            builder.RegisterType<MoviesService>().As<IMoviesService>();
            builder.RegisterType<ReviewsService>().As<IReviewsService>();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}
