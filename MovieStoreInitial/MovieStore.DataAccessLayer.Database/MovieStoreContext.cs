﻿using MovieStore.Models;
using System.Data.Entity;

namespace MovieStore.DataAccessLayer.Database
{
    public class MovieStoreContext : DbContext
    {
        public MovieStoreContext() : base()
        {
            bool instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance != null;
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Log> Logs { get; set; }
    }
}
