namespace MovieStore.DataAccessLayer.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Movies",
                c => new
                    {
                        MovieId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ImdbRating = c.Double(nullable: false),
                        ImdbNumVotes = c.Int(nullable: false),
                        RunningTime = c.Int(nullable: false),
                        DirectedBy = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ShortPlot = c.String(),
                        Plot = c.String(),
                        ReleaseDate = c.DateTime(nullable: false),
                        Poster = c.String(),
                    })
                .PrimaryKey(t => t.MovieId);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ReviewId = c.Int(nullable: false, identity: true),
                        MovieId = c.Int(nullable: false),
                        Author = c.String(),
                        ReviewDate = c.DateTime(nullable: false),
                        Rating = c.Int(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.ReviewId)
                .ForeignKey("dbo.Movies", t => t.MovieId, cascadeDelete: true)
                .Index(t => t.MovieId);
            
            CreateTable(
                "dbo.MovieCategories",
                c => new
                    {
                        Movie_MovieId = c.Int(nullable: false),
                        Category_CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Movie_MovieId, t.Category_CategoryId })
                .ForeignKey("dbo.Movies", t => t.Movie_MovieId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId, cascadeDelete: true)
                .Index(t => t.Movie_MovieId)
                .Index(t => t.Category_CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "MovieId", "dbo.Movies");
            DropForeignKey("dbo.MovieCategories", "Category_CategoryId", "dbo.Categories");
            DropForeignKey("dbo.MovieCategories", "Movie_MovieId", "dbo.Movies");
            DropIndex("dbo.MovieCategories", new[] { "Category_CategoryId" });
            DropIndex("dbo.MovieCategories", new[] { "Movie_MovieId" });
            DropIndex("dbo.Reviews", new[] { "MovieId" });
            DropTable("dbo.MovieCategories");
            DropTable("dbo.Reviews");
            DropTable("dbo.Movies");
            DropTable("dbo.Categories");
        }
    }
}
