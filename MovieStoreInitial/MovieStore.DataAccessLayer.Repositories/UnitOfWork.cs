﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieStore.Models;

namespace MovieStore.DataAccessLayer.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
            Movies = new MoviesRepository(_context);
            Categories = new CategoriesRepository(_context);
            Reviews = new GenericRepository<Review>(_context);
            Logs = new GenericRepository<Log>(_context);
        }

        public IMoviesRepository Movies { get; private set; }

        public ICategoriesRepository Categories { get; private set; }

        public IGenericRepository<Review> Reviews { get; private set; }

        public IGenericRepository<Log> Logs { get; private set; }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}
