﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MovieStore.DataAccessLayer.Database;
using MovieStore.Models;

namespace MovieStore.DataAccessLayer.Repositories
{
    public class CategoriesRepository : ICategoriesRepository
    {
        private readonly DbContext _context;

        public CategoriesRepository(DbContext context)
        {
            _context = context;
            _context.Database.Log = s => Debug.WriteLine(s);
        }

        public IEnumerable<Category> GetAll()
        {
            var categories = _context.Set<Category>().ToList();

            return categories;
        }

        public IEnumerable<Category> GetAllIncluding(params Expression<Func<Category, object>>[] property)
        {
            var categories = _context.Set<Category>().AsQueryable();

            foreach (var item in property)
            {
                categories = categories.Include(item);
            }

            return categories.ToList();
        }
    }
}
