﻿using MovieStore.DataAccessLayer.Database;
using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer.Repositories
{
    public class MoviesRepository : GenericRepository<Movie>, IMoviesRepository
    {
        public MoviesRepository(DbContext _context) : base(_context)
        {

        }

        public IEnumerable<Movie> FilterBy(Expression<Func<Movie, bool>> filter)
        {
            var movies = _dbSet.Where(filter).ToList();

            return movies;
        }

        //public IEnumerable<Movie> FilterByIncluding(Expression<Func<Movie, bool>> filter, params Expression<Func<Movie, object>>[] property)
        //{
        //    var movies = _context.Movies.Where(filter);

        //    foreach (var item in property)
        //    {
        //        movies = movies.Include(item);
        //    }

        //    return movies.ToList();
        //}

        public IEnumerable<Movie> GetAll()
        {
            _context.Database.Log = s => Debug.WriteLine(s);

            var movies = _dbSet.ToList();

            return movies;
        }

        //public IEnumerable<Movie> GetAllIncluding(params Expression<Func<Movie,object>>[] property)
        //{
        //    _context.Database.Log = s => Debug.WriteLine(s);

        //    var movies = _context.Movies.AsQueryable();

        //    foreach (var item in property)
        //    {
        //        movies = movies.Include(item);
        //    }

        //    return movies.ToList();
        //}

        public Movie GetById(int id)
        {
            var movie = _dbSet.Find(id);

            return movie;
        }

        public int GetMoviesCount(string categoryName)
        {
            int count = 0;

            if (!string.IsNullOrEmpty(categoryName) && categoryName.ToLower() != "all")
            {
                count = _dbSet.Count(m => m.Categories.Any(c => c.Name == categoryName));
            }
            else
            {
                count = _dbSet.Count();
            }

            return count;
        }

        public IEnumerable<Movie> GetPagedMovies(string categoryName, int page, int itemsPerPage)
        {
            var movies = _dbSet.Include(x => x.Reviews).Include(x => x.Categories);

            if (!string.IsNullOrEmpty(categoryName) && categoryName.ToLower() != "all")
            {
                movies = movies.Where(m => m.Categories.Any(c => c.Name == categoryName));
            }

            if (page > 0 && itemsPerPage > 9)
            {
                movies = movies
                    .OrderBy(m => m.ImdbRating)
                    .ThenBy(m => m.MovieId)
                    .Skip((page - 1) * itemsPerPage)
                    .Take(itemsPerPage);
            }

            return movies.ToList();
        }
    }
}
