﻿using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.Helpers
{
    public static class Utilities
    {
        public static string ConcatenateCategoryNames(IEnumerable<Category> categories)
        {
            string result = string.Join(",", categories.Select(c => c.Name));

            return result;
        }
    }
}
