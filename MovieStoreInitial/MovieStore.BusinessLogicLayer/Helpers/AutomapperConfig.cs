﻿using AutoMapper;
using MovieStore.BusinessLogicLayer.ViewModels;
using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.Helpers
{
    public static class AutomapperConfig
    {
        public static void ConfigureMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Movie, MovieDetailsViewModel>()
                    .ForMember(d => d.AverageUserRating, m => m.MapFrom(s => s.Reviews.Average(r => r.Rating)))
                    .ForMember(d => d.Categories, m => m.MapFrom(s => Utilities.ConcatenateCategoryNames(s.Categories)));

                cfg.CreateMap<Review, DisplayReviewViewModel>();

                cfg.CreateMap<AddReviewViewModel, Review>();

                cfg.CreateMap<Review, AddReviewViewModel>();
            });
        }
    }
}
