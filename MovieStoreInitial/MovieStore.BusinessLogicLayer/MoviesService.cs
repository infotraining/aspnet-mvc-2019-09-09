﻿using MovieStore.BusinessLogicLayer.Interfaces;
using MovieStore.BusinessLogicLayer.Messaging;
using MovieStore.BusinessLogicLayer.ViewModels;
using MovieStore.DataAccessLayer;
using System;
using System.Linq;
using MovieStore.BusinessLogicLayer.Helpers;
using AutoMapper;
using MovieStore.Models;

namespace MovieStore.BusinessLogicLayer
{
    public class MoviesService : IMoviesService
    {
        private readonly IMoviesRepository _moviesRepo;

        public MoviesService(IMoviesRepository moviesRepo)
        {
            _moviesRepo = moviesRepo;
        }

        public GetMovieDetailsResponse GetMovie(GetMovieDetailsRequest request)
        {
            var response = new GetMovieDetailsResponse();

            try
            {
                var movie = _moviesRepo.FilterByIncluding(m => m.MovieId == request.Id, m => m.Reviews).Single();

                response.Model = Mapper.Map<Movie, MovieDetailsViewModel>(movie);

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public GetMovieListResponse GetMovies(GetMovieListRequest request)
        {
            var response = new GetMovieListResponse();

            try
            {
                response.Movies = _moviesRepo.GetPagedMovies(
                                request.PagingInfo.CategoryName,
                                request.PagingInfo.CurrenPage,
                                request.PagingInfo.ItemsPerPage)
                                .Select(m => new MoviesListItemViewModel
                                {
                                    MovieId = m.MovieId,
                                    Poster = m.Poster,
                                    Price = m.Price,
                                    ShortPlot = m.ShortPlot,
                                    AverageUserRating = m.Reviews.Average(r => r.Rating),
                                    Title = m.Title,
                                    Categories = Utilities.ConcatenateCategoryNames(m.Categories)
                                });

                response.PagingInfo = request.PagingInfo;
                response.PagingInfo.TotalItems = _moviesRepo.GetMoviesCount(request.PagingInfo.CategoryName);

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }            

            return response;
        }
    }
}
