﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.ViewModels
{
    public class DisplayReviewViewModel
    {
        public string Author { get; set; }
        public DateTime ReviewDate { get; set; }
        public int Rating { get; set; }
        public string Content { get; set; }
    }
}
