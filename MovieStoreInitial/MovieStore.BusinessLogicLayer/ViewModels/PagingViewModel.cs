﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.ViewModels
{
    public class PagingViewModel
    {
        public PagingViewModel(string categoryName, int currenPage, int itemsPerPage)
        {
            CategoryName = categoryName;
            CurrenPage = currenPage;
            ItemsPerPage = itemsPerPage;
        }

        public string CategoryName { get; set; }
        public int CurrenPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)TotalItems / ItemsPerPage);
            }
        }
    }
}
