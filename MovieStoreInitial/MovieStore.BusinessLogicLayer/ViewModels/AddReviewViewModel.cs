﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.ViewModels
{
    public class AddReviewViewModel
    {
        public AddReviewViewModel()
        {
            RatingList = new SortedDictionary<int, string>();

            for (int i = 1; i <= 10; i++)
            {
                RatingList.Add(i, i.ToString());
            }

            ReviewDate = DateTime.Now;
        }

        [ScaffoldColumn(false)]
        public int MovieId { get; set; }

        [ScaffoldColumn(false)]
        public string Title { get; set; }

        [Required]
        public string Author { get; set; }

        [ScaffoldColumn(false)]
        public DateTime ReviewDate { get; set; }

        [ScaffoldColumn(false)]
        public int Rating { get; set; }
        public string Content { get; set; }

        public IDictionary<int, string> RatingList { get; private set; }
    }
}
