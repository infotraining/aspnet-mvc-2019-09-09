﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.ViewModels
{
    public class MoviesListItemViewModel
    {
        public int MovieId { get; set; }
        public string Poster { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string ShortPlot { get; set; }
        public string Categories { get; set; }
        public double AverageUserRating { get; set; }
    }
}
