﻿using AutoMapper;
using MovieStore.BusinessLogicLayer.Interfaces;
using MovieStore.BusinessLogicLayer.Messaging;
using MovieStore.DataAccessLayer;
using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer
{
    public class ReviewsService : IReviewsService
    {
        private readonly IUnitOfWork _uow;

        public ReviewsService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public GetAddReviewResponse AddReview(GetAddReviewRequest request)
        {
            var response = new GetAddReviewResponse();

            try
            {
                var review = Mapper.Map<Review>(request.Review);

                var added = _uow.Reviews.Add(review);

                _uow.Logs.Add(new Log
                {
                    EntityId = added.ReviewId.ToString(),
                    EntityName = "Review",
                    ActionType = ActionType.Add
                });

                _uow.Complete();

                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
