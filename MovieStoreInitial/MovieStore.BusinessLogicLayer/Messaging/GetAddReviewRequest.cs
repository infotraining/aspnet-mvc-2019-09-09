﻿using MovieStore.BusinessLogicLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.Messaging
{
    public class GetAddReviewRequest
    {
        public AddReviewViewModel Review { get; set; }
    }

    public class GetAddReviewResponse : ResponseBase
    {
    }

}
