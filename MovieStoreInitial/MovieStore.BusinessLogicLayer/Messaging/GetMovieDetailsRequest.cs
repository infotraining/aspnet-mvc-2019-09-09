﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.Messaging
{
    public class GetMovieDetailsRequest
    {
        public int Id { get; set; }
    }
}
