﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieStore.BusinessLogicLayer.ViewModels;

namespace MovieStore.BusinessLogicLayer.Messaging
{
    public class GetMovieDetailsResponse : ResponseBase
    {
        public MovieDetailsViewModel Model { get; set; }
    }
}
