﻿using MovieStore.BusinessLogicLayer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.Messaging
{
    public class GetMovieListRequest
    {
        public PagingViewModel PagingInfo { get; set; }
    }
}
