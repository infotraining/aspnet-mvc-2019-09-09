﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovieStore.BusinessLogicLayer.Messaging;

namespace MovieStore.BusinessLogicLayer.Interfaces
{
    public interface IMoviesService
    {
        GetMovieListResponse GetMovies(GetMovieListRequest request);
        GetMovieDetailsResponse GetMovie(GetMovieDetailsRequest request);
    }
}
