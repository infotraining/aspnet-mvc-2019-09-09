﻿using MovieStore.BusinessLogicLayer.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.BusinessLogicLayer.Interfaces
{
    public interface IReviewsService
    {
        GetAddReviewResponse AddReview(GetAddReviewRequest request);
    }
}
