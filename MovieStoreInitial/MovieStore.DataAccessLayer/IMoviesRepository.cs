﻿using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MovieStore.DataAccessLayer
{
    public interface IMoviesRepository : IGenericRepository<Movie>
    {
        IEnumerable<Movie> GetPagedMovies(string categoryName, int page, int itemsPerPage);
        int GetMoviesCount(string categoryName);

        IEnumerable<Movie> GetAll();
        IEnumerable<Movie> FilterBy(Expression<Func<Movie, bool>> filter);
    }
}
