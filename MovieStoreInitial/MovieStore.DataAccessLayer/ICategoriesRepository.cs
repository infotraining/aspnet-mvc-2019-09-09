﻿using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MovieStore.DataAccessLayer
{
    public interface ICategoriesRepository
    {
        IEnumerable<Category> GetAll();
        IEnumerable<Category> GetAllIncluding(params Expression<Func<Category, object>>[] property);
    }
}
