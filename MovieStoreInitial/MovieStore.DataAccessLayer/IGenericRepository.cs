﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] properties);
        IEnumerable<TEntity> FilterByIncluding(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] properties);
        TEntity GetById(object id);

        //void Add(TEntity entity);
        TEntity Add(TEntity entity);
        void Edit(TEntity entity);
        void Remove(object id);

        //void Complete();
    }
}
