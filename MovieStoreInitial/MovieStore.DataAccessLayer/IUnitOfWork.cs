﻿using MovieStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.DataAccessLayer
{
    public interface IUnitOfWork
    {
        void Complete();

        IMoviesRepository Movies { get; }
        ICategoriesRepository Categories { get; }
        IGenericRepository<Review> Reviews { get; }
        IGenericRepository<Log> Logs { get; }
    }
}
