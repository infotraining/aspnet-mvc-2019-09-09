﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Models
{
    public class Review
    {
        public int ReviewId { get; set; }
        public int MovieId { get; set; }
        public string Author { get; set; }
        public DateTime ReviewDate { get; set; }
        public int Rating { get; set; }
        public string Content { get; set; }
        public Movie Movie { get; set; }
    }
}
