﻿using System;
using System.Collections.Generic;

namespace MovieStore.Models
{
    public class Movie
    {
        public Movie()
        {
            Reviews = new List<Review>();
            Categories = new List<Category>();
        }

        public int MovieId { get; set; }
        public string Title { get; set; }
        public double ImdbRating { get; set; }
        public int ImdbNumVotes { get; set; }
        public int RunningTime { get; set; }
        public string DirectedBy { get; set; }
        public decimal Price { get; set; }
        public string ShortPlot { get; set; }
        public string Plot { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Poster { get; set; }

        public IList<Category> Categories { get; set; }
        public IList<Review> Reviews { get; set; }
    }
}
