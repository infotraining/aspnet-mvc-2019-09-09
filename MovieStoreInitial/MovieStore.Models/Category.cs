﻿using System.Collections.Generic;

namespace MovieStore.Models
{
    public class Category
    {
        public Category()
        {
            Movies = new List<Movie>();
        }

        public int CategoryId { get; set; }
        public string Name { get; set; }

        public IList<Movie> Movies { get; set; }
    }
}
