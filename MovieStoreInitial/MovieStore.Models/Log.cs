﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieStore.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string EntityName { get; set; }
        public string EntityId { get; set; }
        public ActionType ActionType { get; set; }
    }

    public enum ActionType
    {
        Read,
        Add,
        Edit,
        Remove
    }
}
